
import { COLOR_1, COLOR_2 } from '../variables/variables';
import { pointer } from '../helpers/helpers.styled';
import styled from 'styled-components';

export const BaseButton = styled.button`
  ${ pointer }
  background-color: ${ COLOR_1 };
  border: none;
  border-radius: 5px;
  color: ${ COLOR_2 };
  display: flex;
  font-weight: 300;
  height: 35px;
  justify-content: center;
  line-height: 35px;
  padding: 0 10px;
  text-transform: uppercase;
  width: fit-content;
`;

export const OutlineButton = styled(BaseButton)`
  background-color: transparent;
  border: 1px solid ${ COLOR_1 };
  color: ${ COLOR_1 };
`;
