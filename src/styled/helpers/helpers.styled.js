
import { parseToRgb, rgba } from 'polished';
import { css } from 'styled-components';

import { COLOR_2 } from '../variables/variables';

export const mqTablet = (...style) => css`
  @media (min-width: 768px) {
    ${ css(...style) };
  }
`;

export const mqDesktop = (...style) => css`
  @media (min-width: 1080px) {
    ${ css(...style) };
  }
`;

export const dim = (amount = 0.7, color = COLOR_2) => css`
  background-color: ${ rgba({ ...parseToRgb(color), alpha: amount }) };
`;

export const pointer = css`
  &, * {
    cursor: pointer;
  }
`;
