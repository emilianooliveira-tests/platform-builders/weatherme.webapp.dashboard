
import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { getReducers } from '../reducers/reducers';
import { getSagas } from '../sagas/sagas';

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  getReducers(),
  applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(getSagas);
