
import { combineReducers } from 'redux';

import weatherDataReducer from './weather/weather.data.reducer';

export default combineReducers({
  weather: weatherDataReducer,
});