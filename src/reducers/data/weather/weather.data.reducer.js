
import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  updateDailyWeather: (payload = { }) => ({
    payload,
    type: 'UPDATE_DAILY_WEATHER',
  }),
});

const INITIAL_STATE = {
  address: null,
  daily: null,
  geolocation: null,
  lastUpdate: null,
};

const updateDailyWeather = (state = INITIAL_STATE, action) => ({
  ...state,
  address: { ...action.payload.address },
  daily: [...action.payload.daily],
  geolocation: { ...action.payload.geolocation },
  lastUpdate: new Date(),
});

export default createReducer(INITIAL_STATE, {
  [Types.UPDATE_DAILY_WEATHER]: updateDailyWeather,
});