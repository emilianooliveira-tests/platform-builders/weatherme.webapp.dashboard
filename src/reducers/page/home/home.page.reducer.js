
import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  resetHomePage: (payload = { }) => ({
    payload,
    type: 'RESET_HOME_PAGE',
  }),
  updateHomePage: (payload = { }) => ({
    payload,
    type: 'UPDATE_HOME_PAGE',
  }),
});

const INITIAL_STATE = {
  geolocationNotAllowedError: false,
  isLoadingWeather: false,
  unknownError: false,
};

const resetHomePage = () => ({
  ...INITIAL_STATE,
});

const updateHomePage = (state = INITIAL_STATE, action) => ({
  ...state,
  ...action.payload,
});

export default createReducer(INITIAL_STATE, {
  [Types.RESET_HOME_PAGE]: resetHomePage,
  [Types.UPDATE_HOME_PAGE]: updateHomePage,
});
