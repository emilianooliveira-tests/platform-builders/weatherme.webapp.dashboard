
import { combineReducers } from 'redux';

import discoverPageReducer from './discover/discover.page.reducer';
import homePageReducer from './home/home.page.reducer';

export default combineReducers({
  discover: discoverPageReducer,
  home: homePageReducer,
});
