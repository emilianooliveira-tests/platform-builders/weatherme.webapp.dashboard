
import { createActions, createReducer } from 'reduxsauce';

export const { Types, Creators } = createActions({
  resetDiscoverPage: (payload = { }) => ({
    payload,
    type: 'RESET_DISCOVER_PAGE',
  }),
  updateDiscoverPage: (payload = { }) => ({
    payload,
    type: 'UPDATE_DISCOVER_PAGE',
  }),
});

const INITIAL_STATE = {
  isLoadingWeather: false,
};

const resetDiscoverPage = () => ({
  ...INITIAL_STATE,
});

const updateDiscoverPage = (state = INITIAL_STATE, action) => ({
  ...state,
  ...action.payload,
});

export default createReducer(INITIAL_STATE, {
  [Types.RESET_DISCOVER_PAGE]: resetDiscoverPage,
  [Types.UPDATE_DISCOVER_PAGE]: updateDiscoverPage,
});
