
import { combineReducers } from 'redux';

import dataReducer from './data/data.reducer';
import pageReducer from './page/page.reducer';

export function getReducers () {
  return  combineReducers({
    data: dataReducer,
    page: pageReducer,
  });
}