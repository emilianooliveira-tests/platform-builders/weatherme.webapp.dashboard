
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';

import { COLOR_1 } from '../../../styled/variables/variables';
import { Creators as HomePageActions } from '../../../reducers/page/home/home.page.reducer';
import LoadingButton from '../../../components/loading-button/LoadingButton';
import { Creators as WeatherSagaActions } from '../../../sagas/weather/weather.saga';

/*
  global
  GeolocationPositionError
*/

function AccessForecastButton () {

  const { updateHomePage, asyncGetWeather } = bindActionCreators({
    asyncGetWeather: WeatherSagaActions.asyncGetWeather,
    updateHomePage: HomePageActions.updateHomePage,
  }, useDispatch());

  const { isLoadingWeather } = useSelector(state => ({
    isLoadingWeather: state.page.home.isLoadingWeather,
  }));

  const history = useHistory();

  const handleClick = async () => {
    updateHomePage({
      isLoadingWeather: true,
    });

    asyncGetWeather({ }, {
      onError: error => {
        if(error instanceof GeolocationPositionError) {
          updateHomePage({
            geolocationNotAllowedError: true,
            isLoadingWeather: false,
            unknownError: false,
          });
        } else {
          updateHomePage({
            geolocationNotAllowedError: false,
            isLoadingWeather: false,
            unknownError: true,
          });
        }
      },
      onSuccess: () => {
        updateHomePage({
          geolocationNotAllowedError: false,
          isLoadingWeather: false,
          unknownError: false,
        });

        setTimeout(() => {
          updateHomePage({ isLoadingWeather: false });
          history.push('/discover');
        }, 250);
      },
    });
  };

  return (
    <Wrapper
      isLoading={ isLoadingWeather }
      onClick={ handleClick }
    >
      check forecast
    </Wrapper>
  );
}

const Wrapper = styled(LoadingButton)`
  align-self: center;
  background-color: transparent;
  border: 1px solid ${ COLOR_1 };
  color: ${ COLOR_1 };
  margin-bottom: 20px;

  svg {
    color: ${ COLOR_1 };
  }
`;

export default AccessForecastButton;
