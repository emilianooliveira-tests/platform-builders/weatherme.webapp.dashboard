
import styled, { css } from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';

import { dim, mqTablet } from '../../styled/helpers/helpers.styled';
import AccessForecastButton from './access-forecast-button/AccessForecastButton';
import { COLOR_1 } from '../../styled/variables/variables';
import Cover from '../../components/cover/Cover';
import { Creators as HomePageActions } from '../../reducers/page/home/home.page.reducer';
import Logo from '../../components/logo/Logo';
import { useEffect } from 'react';

function Home () {

  const { resetHomePage } = bindActionCreators({
    resetHomePage: HomePageActions.resetHomePage,
  }, useDispatch());

  useEffect(() => resetHomePage, [ ]);

  const { geolocationNotAllowedError, unknownError } = useSelector(state => ({
    geolocationNotAllowedError: state.page.home.geolocationNotAllowedError,
    unknownError: state.page.home.unknownError,
  }));

  return (
    <Wrapper>
      <HCover
        url={`${process.env.PUBLIC_URL}/videos/covers/clear.mp4`}
        placeholderUrl={`${process.env.PUBLIC_URL}/videos/covers/clear.png`}
      />
      <ContentWrapper>
        <HLogo />
        <AccessForecastButton />
        {/* Error messages bellow */}
        <>
          <ErrorMessage show={ geolocationNotAllowedError }>
            Oops! Geolocation disabled.
            <br />
            Access your browser's permissions and allow geolocation access.
          </ErrorMessage>
          <ErrorMessage show={ unknownError }>
            Oops! We're having problems.
            <br />
            Please try again later.
          </ErrorMessage>
        </>
      </ContentWrapper>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${ dim(0.5) }
  height: 100vh;
  justify-content: center;
  position: relative;
  width: 100vw;
`;

const HCover = styled(Cover)`
  position: fixed;
  top: 0;
  z-index: -999;
`;

const ContentWrapper = styled.div`
  align-self: center;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  padding: 20px;
  position: fixed;
  top: 0;
  width: 100%;

  ${ mqTablet`
    padding-bottom: 40px;
  ` }
`;

const HLogo = styled(Logo).attrs({
  scale: {
    mobile: 1.75,
    tablet: 2,
  },
})`
  align-self: center;
  margin-bottom: 30px;

  ${ mqTablet`
    margin-bottom: 40px;
  ` }
`;

const ErrorMessage = styled.label`
  align-self: center;
  color: ${ COLOR_1 };
  font-weight: 300;
  line-height: 22px;
  max-height: 0px;
  max-width: 500px;
  opacity: 0;
  text-align: center;
  transition: ease .6s;

  ${ ({ show }) => show && css`
    max-height: 45px;
    opacity: 1;
  ` }
`;

export default Home;
