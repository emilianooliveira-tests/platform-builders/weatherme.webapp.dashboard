
import propTypes from 'prop-types';
import styled from 'styled-components';

import { COLOR_1 } from '../../../styled/variables/variables';
import { LocationOn } from '@styled-icons/material-rounded/LocationOn';
import { MONTHS } from '../../../constants/months/months.contants';
import { WEEK_DAYS } from '../../../constants/week-days/week-days.constants';
import { useSelector } from 'react-redux';

function Location () {

  const { daily, address } = useSelector(state => ({
    address: state.data.weather.address,
    daily: state.data.weather.daily,
  }));

  let weekDay, day, month, year;
  if(daily?.length > 0) {
    const today = daily[0].date;
    weekDay = WEEK_DAYS[today.getDay()];
    day = today.getDate();
    month = MONTHS[today.getMonth()];
    year = today.getFullYear();
  }

  return (
    <Wrapper>
      <City>
        { address?.city }
      </City>
      <Date>
        { `${ weekDay }, ${ day } ${ month } ${ year }` }
      </Date>
      <CountryAndStateWrapper>
        <LocationIcon />
        <CountryAndState>
          { address?.state }
          { address?.state && address?.country && ' - ' }
          { address?.country }
        </CountryAndState>
      </CountryAndStateWrapper>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  align-self: flex-start;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;

  transition: 0.3s ease;
`;

const City = styled.label`
  align-self: flex-end;
  color: ${ COLOR_1 };
  display: block;
  font-size: 22px;
  font-weight: 300;
  margin-bottom: 5px;
`;

const CountryAndStateWrapper = styled.div`
  align-self: flex-end;
  display: flex;
`;

const Date = styled(City)`
  font-size: 14px;
  opacity: 0.5;
`;

const CountryAndState = styled(Date)`
  align-self: center;
  font-size: 12px;
  margin: 0;
  margin: 0;
`;

const LocationIcon = styled(LocationOn)`
  align-self: center;
  color: ${ COLOR_1 };
  height: 14px;
  margin-right: 3px;
  opacity: 0.6;
`;

Location.propTypes = {
  date: propTypes.object,
};

export default Location;