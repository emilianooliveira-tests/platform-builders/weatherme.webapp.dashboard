
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';

import { dim, mqTablet } from '../../styled/helpers/helpers.styled';
import Cover from '../../components/cover/Cover';
import CurrentWeather from './current-weather/CurrentWeather';
import { Creators as DiscoverPageActions } from '../../reducers/page/discover/discover.page.reducer';
import Location from './location/Location';
import RefreshButton from './refresh-button/RefreshButton';
import Weather from './weather/Weather';

const buildCover = ({ name }) => ({
  placeholderUrl: `${process.env.PUBLIC_URL}/videos/covers/${name}.png`,
  url: `${process.env.PUBLIC_URL}/videos/covers/${name}.mp4`,
});

function Discover () {

  const { resetDiscoverPage } = bindActionCreators({
    resetDiscoverPage: DiscoverPageActions.resetDiscoverPage,
  }, useDispatch());

  const history = useHistory();

  const { daily } = useSelector(state => ({
    daily: state.data.weather.daily,
  }));

  const [state, setState] = useState({
    cover: buildCover({ name: 'clear' }),
  });

  useEffect(() => resetDiscoverPage, [ ]);

  useEffect(() => {
    if(daily?.length) return;
    history.push('/');
  }, [ ]);

  useEffect(() => {
    if(!daily) return;

    setState(prev => ({
      ...prev,
      cover: buildCover({ name: daily[0].description.toLowerCase() }),
    }));
  }, [ daily ]);

  return (
    <Wrapper>
      <DCover { ...state.cover } />
      <ContentWrapper>
        <TopWrapper>
          <RefreshButton />
          <Location />
        </TopWrapper>
        <CurrentWeather />
        <BottomWrapper>
          { daily?.slice(1, 7).map(d => (
            <Weather
              key={d.date.getTime()}
              { ...d }
            />
          )) }
        </BottomWrapper>
      </ContentWrapper>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  ${ dim(0.5) }
  height: 100vh;
  justify-content: center;
  position: relative;
  width: 100vw;
`;

const DCover = styled(Cover)`
  position: fixed;
  top: 0;
  z-index: -999;
`;

const ContentWrapper = styled.div`
  align-self: center;
  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: space-between;
  padding: 20px;
  position: fixed;
  top: 0;
  width: 100%;

  ${ mqTablet`
    padding-bottom: 40px;
  ` }
`;

const TopWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const BottomWrapper = styled(TopWrapper)`
  align-self: center;
  flex-wrap: wrap;
  justify-content: center;
  transition: .3s ease;
  width: fit-content;
`;

export default Discover;
