
import propTypes from 'prop-types';
import styled from 'styled-components';

import { COLOR_1 } from '../../../styled/variables/variables';
import WeatherIcon from '../../../components/weather-icon/WeatherIcon';
import { mqTablet } from '../../../styled/helpers/helpers.styled';
import { useSelector } from 'react-redux';

function CurrentWeather () {

  const { daily, lastUpdate } = useSelector(state => ({
    daily: state.data.weather.daily,
    lastUpdate: state.data.weather.lastUpdate,
  }));

  let today;
  if(daily?.length > 0) today = daily[0];

  return (
    <Wrapper>
      <TopWrapper>
        <Temperature>
          { `${parseInt(today?.current)}°` }
        </Temperature>
        <Icon type={ today?.description  } />
      </TopWrapper>
      <BottomWrapper>
        <MinTemperature>
        { `min ${parseInt(today?.min)}°` }
        </MinTemperature>
        <Split>
          /
        </Split>
        <MaxTemperature>
        { `max ${parseInt(today?.max)}°` }
        </MaxTemperature>
      </BottomWrapper>
      <LastUpdate>
        Last update at { lastUpdate?.toLocaleTimeString() }
      </LastUpdate>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  align-self: center;
  display: flex;
  flex-direction: column;
  justify-content: center;

  ${ mqTablet`
    transition: ease 0.1s;
    &:hover {
      transform: scale(1.3);
    }
  ` }
`;

const TopWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 10px;
`;

const Temperature = styled.label`
  align-self: center;
  color: ${ COLOR_1 };
  display: block;
  font-size: 60px;
  font-weight: 200;
  line-height: 50px;
  margin-right: 10px;
`;

const Icon = styled(WeatherIcon)`
  align-self: center;
  svg {
    color: ${ COLOR_1 };
    height: 24px;
  }
`;

const BottomWrapper = styled(TopWrapper)`
  margin-bottom: 5px;
`;

const MinTemperature = styled(Temperature)`
  font-size: 14px;
  font-weight: unset;
  line-height: unset;
  margin: 0;
  opacity: 0.5;
`;

const Split = styled(MinTemperature)`
  margin: 0 10px;
`;

const MaxTemperature = styled(MinTemperature)``;

const LastUpdate = styled(MinTemperature)`
  align-self: center;
  font-size: 12px;
  line-height: 10px;

  ${ mqTablet`
    display none;
  ` }
`;

CurrentWeather.propTypes = {
  className: propTypes.string,
  current: propTypes.number,
  description: propTypes.string,
  lastUpdate: propTypes.object,
  max: propTypes.number,
  min: propTypes.number,
};

export default CurrentWeather;