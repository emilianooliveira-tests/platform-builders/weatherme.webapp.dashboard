
import styled, { css } from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { Refresh } from '@styled-icons/material-rounded';
import { bindActionCreators } from 'redux';
import { useHistory } from 'react-router-dom';

import { mqTablet, pointer } from '../../../styled/helpers/helpers.styled';
import { COLOR_1 } from '../../../styled/variables/variables';
import { Creators as DiscoverPageActions } from '../../../reducers/page/discover/discover.page.reducer';
import { Creators as HomePageActions } from '../../../reducers/page/home/home.page.reducer';
import { Creators as WeatherSagaActions } from '../../../sagas/weather/weather.saga';

/*
  global
  GeolocationPositionError
*/

function RefreshButton () {

  const {
    asyncGetWeather,
    updateDiscoverPage,
    updateHomePage,
  } = bindActionCreators({
    asyncGetWeather: WeatherSagaActions.asyncGetWeather,
    updateDiscoverPage: DiscoverPageActions.updateDiscoverPage,
    updateHomePage: HomePageActions.updateHomePage,
  }, useDispatch());

  const history = useHistory();

  const {
    isLoadingWeather,
    lastUpdate,
  } = useSelector(state => ({
    isLoadingWeather: state.page.discover.isLoadingWeather,
    lastUpdate: state.data.weather.lastUpdate,
  }));

  const handleClick = () => {
    updateDiscoverPage({
      isLoadingWeather: true,
    });

    asyncGetWeather({ }, {
      onError: error => {
        if(error instanceof GeolocationPositionError) {
          updateHomePage({
            geolocationNotAllowedError: true,
            isLoadingWeather: false,
            unknownError: false,
          });
        } else {
          updateHomePage({
            geolocationNotAllowedError: false,
            isLoadingWeather: false,
            unknownError: true,
          });
        }

        history.push('/');
      },
      onSuccess: () => updateDiscoverPage({
        isLoadingWeather: false,
      }),
    });
  };

  return (
    <Wrapper onClick={ handleClick }>
      <Icon isRotating={ isLoadingWeather } />
      <LastUpdate>
        Last update at { lastUpdate?.toLocaleTimeString() }
      </LastUpdate>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  ${ pointer }
  align-self: flex-start;
  transition: 0.3s ease;
`;

const Icon = styled(Refresh)`
  align-self: center;
  animation-duration: 1000ms;
  animation-iteration-count: infinite;
  animation-name: spin;
  animation-play-state: paused;
  animation-timing-function: linear;
  color: ${ COLOR_1 };
  height: 30px;
  opacity: 0.8;
  transform: scale(1);
  transition: ease 0.3s;

  ${ ({ isRotating }) => isRotating && css`
    animation-play-state: running;
    height: 50px;
  ` }

  ${ mqTablet`
    margin-right: 10px;
  ` }

  @keyframes spin { 
    from { 
      transform: rotate(0deg); 
    } to { 
      transform: rotate(360deg); 
    }
  }
`;

const LastUpdate = styled.label`
  display: none;

  ${ mqTablet`
    align-self: center;
    color: ${ COLOR_1 };
    display: unset;
    font-size: 12px;
    line-height: 10px;
    opacity: 0.5;
  ` }
`;

export default RefreshButton;