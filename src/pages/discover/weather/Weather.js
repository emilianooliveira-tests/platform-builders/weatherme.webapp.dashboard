
import propTypes from 'prop-types';
import styled from 'styled-components';

import { COLOR_1 } from '../../../styled/variables/variables';
import { MONTHS } from '../../../constants/months/months.contants';
import { WEEK_DAYS } from '../../../constants/week-days/week-days.constants';
import WeatherIcon from '../../../components/weather-icon/WeatherIcon';
import { mqTablet } from '../../../styled/helpers/helpers.styled';

function Weather ({
  date,
  description,
}) {

  return (
    <Wrapper>
      <Icon type={description} />
      <Weekday>
        { WEEK_DAYS[date.getDay()] }
      </Weekday>
      <Date>
        { `${MONTHS[date.getMonth()]} ${date.getDate()}` }
      </Date>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  align-self: flex-end;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 7px;
  margin-bottom: 0;

  ${ mqTablet`
    transition: ease 0.1s;
    margin: 15px;
    margin-bottom: 0;
    &:hover {
      transform: scale(1.3);
    }
  ` }
`;

const Icon = styled(WeatherIcon)`
  align-self: center;
  margin-bottom: 5px;

  svg {
    color: ${ COLOR_1 };
    height: 22px;
  }
`;

const Weekday = styled.label`
  align-self: center;
  color: ${ COLOR_1 };
  font-size: 14px;
`;

const Date = styled(Weekday)`
  font-size: 12px;
  opacity: 0.5;
`;

Weather.propTypes = {
  date: propTypes.object,
  description: propTypes.string,
};

export default Weather;