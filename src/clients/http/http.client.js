
import axios from 'axios';

export const get = ({
  url,
  path,
  params,
  options = { },
}) => {
  return axios.get(url + path, {
    params,
    ...options,
  });
};