
import propTypes from 'prop-types';
import styled from 'styled-components';

function Cover ({ className, url, placeholderUrl }) {

  return (
    <Wrapper className={ className }>
      <Video key={url} poster={ placeholderUrl }>
        <Source src={ url } />
      </Video>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  height: 100%;
  overflow: hidden;
  width: 100%;
`;

const Video = styled.video.attrs({
  autoPlay: process.env.NODE_ENV === 'production',
  // autoPlay: true,
  loop: true,
  muted: true,
  playsInline: true,
})`
  height: 100%;
  width: 100%;
  object-fit: cover;
`;

const Source = styled.source.attrs({
  type: 'video/mp4',
})``;

Cover.propTypes = {
  className: propTypes.string,
  placeholderUrl: propTypes.string,
  url: propTypes.string,
};

export default Cover;