
import styled, { css } from 'styled-components';
import { CloudSun } from '@styled-icons/fa-solid';
import propTypes from 'prop-types';

import { COLOR_1 } from '../../styled/variables/variables';
import { mqTablet } from '../../styled/helpers/helpers.styled';

function Logo ({ className, scale }) {
  return (
    <Wrapper className={ className }>
      <Icon scale={ scale } />
      <Name scale={ scale }>
        WeatherMe
      </Name>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
`;

const Icon = styled(CloudSun)`
  align-self: center;
  color: ${ COLOR_1 };
  
  ${ ({ scale }) => css`
    height: ${ scale.mobile * 30 }px;
    margin-right: ${ scale.mobile * 5 }px;

    ${ mqTablet`
      height: ${ scale.tablet * 30 }px;
      margin-right: ${ scale.tablet * 5 }px;
    ` }
  ` }
`;

const Name = styled.label`
  align-self: center;
  color: ${ COLOR_1 };
  font-weight: 200;

  ${ ({ scale }) => css`
    font-size: ${ scale.mobile * 24 }px;

    ${ mqTablet`
      font-size: ${ scale.tablet * 24 }px;
    ` }
  ` }
`;

Logo.propTypes = {
  className: propTypes.string,
  scale: propTypes.shape({
    mobile: propTypes.number,
    tablet: propTypes.number,
  }),
};

Logo.defaultProps = {
  scale: {
    mobile: 1,
    tablet: 1,
  },
};

export default Logo;
