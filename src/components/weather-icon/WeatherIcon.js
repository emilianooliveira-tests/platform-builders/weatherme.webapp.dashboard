
import { CloudRain, CloudSun, CloudSunRain, Snowflake, Sun } from '@styled-icons/fa-solid';
import styled, { css } from 'styled-components';
import { Thunderstorm } from '@styled-icons/ionicons-sharp';
import propTypes from 'prop-types';

import { COLOR_2 } from '../../styled/variables/variables';

function WeatherIcon ({ className, type }) {

  let Icon;
  switch (type) {
    case WeatherIcon.TYPES.CLOUDS: Icon = CloudsIcon; break;
    case WeatherIcon.TYPES.DRIZZLE: Icon = DrizzleIcon; break;
    case WeatherIcon.TYPES.RAIN: Icon = RainIcon; break;
    case WeatherIcon.TYPES.SNOW: Icon = SnowIcon; break;
    case WeatherIcon.TYPES.THUNDERSTORM: Icon = ThunderstormIcon; break;
    case WeatherIcon.TYPES.CLEAR:
    default: Icon = ClearIcon; break;
  }

  return (
    <Wrapper className={className}>
      <Icon />
    </Wrapper>
  );
}

WeatherIcon.TYPES = {
  CLEAR: 'CLEAR',
  CLOUDS: 'CLOUDS',
  DRIZZLE: 'DRIZZLE',
  RAIN: 'RAIN',
  SNOW: 'SNOW',
  THUNDERSTORM: 'THUNDERSTORM',
};

const Wrapper = styled.div``;

const iconBaseStyle = css`
  color: ${ COLOR_2 };
  height: 20px;
`;

const ClearIcon = styled(Sun)`
  ${iconBaseStyle}
`;

const CloudsIcon = styled(CloudSun)`
  ${iconBaseStyle}
`;

const DrizzleIcon = styled(CloudSunRain)`
  ${iconBaseStyle}
`;

const RainIcon = styled(CloudRain)`
  ${iconBaseStyle}
`;

const SnowIcon = styled(Snowflake)`
  ${iconBaseStyle}
`;

const ThunderstormIcon = styled(Thunderstorm)`
  ${iconBaseStyle}
`;

WeatherIcon.propTypes = {
  className: propTypes.string,
  type: propTypes.oneOf([
    WeatherIcon.TYPES.CLEAR,
    WeatherIcon.TYPES.CLOUDS,
    WeatherIcon.TYPES.DRIZZLE,
    WeatherIcon.TYPES.RAIN,
    WeatherIcon.TYPES.SNOW,
    WeatherIcon.TYPES.THUNDERSTORM,
  ]),
};

export default WeatherIcon;