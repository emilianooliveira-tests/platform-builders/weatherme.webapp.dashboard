
import styled, { css } from 'styled-components';
import { Refresh } from '@styled-icons/material-rounded';
import propTypes from 'prop-types';

import { BaseButton } from '../../styled/buttons/buttons.styled';
import { COLOR_2 } from '../../styled/variables/variables';

function LoadingButton ({
  children,
  className,
  isLoading,
  onClick,
}) {

  return (
    <Wrapper
      className={ className }
      onClick={ onClick }
    >
      { children }
      <Icon show={ isLoading } />
    </Wrapper>
  );
}

const Wrapper = styled(BaseButton)`
  align-self: center;
  margin-bottom: 20px;
  transition: ease 0.3s;
`;

const Icon = styled(Refresh)`
  align-self: center;
  animation-duration: 1000ms;
  animation-iteration-count: infinite;
  animation-name: spin;
  animation-play-state: paused;
  animation-timing-function: linear;
  color: ${ COLOR_2 };
  height: 20px;
  margin-left: -20px;
  opacity: 0;
  transition: ease 0.3s;

  ${ ({ show }) => show && css`
    animation-play-state: running;
    margin-left: 5px;
    opacity: 1;
  ` }

  @keyframes spin { 
    from { 
      transform: rotate(0deg); 
    } to { 
      transform: rotate(360deg); 
    }
  }
`;

LoadingButton.propTypes = {
  children: propTypes.string.isRequired,
  className: propTypes.string.isRequired,
  isLoading: propTypes.bool.isRequired,
  onClick: propTypes.func.isRequired,
};

export default LoadingButton;