
import { Provider } from 'react-redux';
import React from 'react';
import propTypes from 'prop-types';

import { store } from '../../store/store';

function StoreProvider ({ children }) {
  return (
    <Provider store={ store }>
      { children }
    </Provider>
  );
}

StoreProvider.propTypes = {
  children: propTypes.array,
};

export default StoreProvider;