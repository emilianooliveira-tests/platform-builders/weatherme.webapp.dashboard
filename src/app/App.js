
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import Fader from 'react-fader';
import Switch from 'react-router-transition-switch';

import Discover from '../pages/discover/Discover';
import Home from '../pages/home/Home';
import StoreProvider from '../hoc/store-provider/StoreProvider';
import { StylingResetter } from '../styled/globals/globals.styled';

function App () {
  return (
    <StoreProvider>
      <StylingResetter />
      <BrowserRouter>
      <Switch component={ Fader }>
        <Route exact path="/" component={Home} />
        <Route path="/discover" component={Discover} />
        <Redirect to="/" />
      </Switch>
      </BrowserRouter>
    </StoreProvider>
  );
}

export default App;
