
import { all } from 'redux-saga/effects';

import getWeatherSagas from './weather/weather.saga';

export function* getSagas () {
  yield all([
    ...getWeatherSagas(),
  ]);
}