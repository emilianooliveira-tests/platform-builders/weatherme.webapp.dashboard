
import { createActions } from 'reduxsauce';
import { takeLatest } from 'redux-saga/effects';

import asyncGetWeather from './async-get-weather/async-get-weather.weather.saga';

export const { Types, Creators } = createActions({
  asyncGetWeather: (payload = { }, callbacks = { }) => ({
    callbacks,
    payload,
    type: 'ASYNC_GET_WEATHER',
  }),
});

export default function getSagas () {
  return [
    takeLatest(Types.ASYNC_GET_WEATHER, asyncGetWeather),
  ];
}