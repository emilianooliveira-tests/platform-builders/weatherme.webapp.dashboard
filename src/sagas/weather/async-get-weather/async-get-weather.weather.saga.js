
import { call, put } from 'redux-saga/effects';

import { getAddress, get as getGeolocation } from '../../../services/geolocation/geolocation.service';
import { Creators as WeatherDataActions } from '../../../reducers/data/weather/weather.data.reducer';
import { byGeolocation as getWeatherByGeolocation } from '../../../services/weather/weather.service';

export default function* asyncGetWeather (action) {

  const { onSuccess, onError } = action.callbacks;

  let geolocation;
  try {
    geolocation = yield call(getGeolocation);
  } catch (error) {
    if(onError) onError(error);
    return;
  }

  let address;
  try {
    address = yield call(getAddress, {
      ...geolocation,
    });
  } catch (error) {
    if(onError) onError(error);
    return;
  }

  // Rio de Janeiro
  // geolocation.latitude = -22.9777054;
  // geolocation.logitude = -43.3657419;

  // Quebec
  // geolocation.latitude = 46.8560266;
  // geolocation.logitude = -71.6218733;

  // Saara
  // geolocation.latitude = 21.9753751;
  // geolocation.longitude = 13.0867956;

  // Alasca
  // geolocation.latitude = 60.1346598;
  // geolocation.longitude = -176.4293931;

  let dailyWeather;
  try {
    dailyWeather = yield call(getWeatherByGeolocation, geolocation);
  } catch (error) {
    if(onError) onError(error);
    return;
  }

  yield put(WeatherDataActions.updateDailyWeather({
    address,
    daily: dailyWeather,
    geolocation,
  }));

  if(onSuccess) onSuccess(dailyWeather);
}