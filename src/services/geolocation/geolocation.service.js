
import Address from './models/address/address.model';
import Geolocation from './models/geolocation/geolocation.model';
import { get as httpGet } from '../../clients/http/http.client';

export const get = () => new Promise((resolve, reject) => {
  navigator.geolocation.getCurrentPosition(result => {
    const { latitude, longitude } = result.coords;
    resolve(new Geolocation({
      latitude,
      longitude,
    }));
  }, reject, {
    enableHighAccuracy: true,
    maximumAge: 10000,
  });
});

export const getAddress = async ({ latitude, longitude }) => {

  const response = await httpGet({
    params: {
      apiKey: process.env.REACT_APP_HERE_API_KEY,
      mode: 'retrieveAddresses',
      prox: `${latitude},${longitude}`,
    },
    path: '/6.2/reversegeocode.json',
    url: process.env.REACT_APP_HERE_REVERSE_GEOCODER_API,
  });

  const { Location } = response.data.Response.View[0].Result[0];

  const result = new Address({
    city: Location.Address.City,
    country: Location.Address.Country,
    state: Location.Address.State,
  });

  return result;
};