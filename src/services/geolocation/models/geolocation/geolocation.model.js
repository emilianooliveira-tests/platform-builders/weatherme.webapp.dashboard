
class Geolocation {

  constructor ({
    latitude,
    longitude,
  }) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

}

export default Geolocation;
