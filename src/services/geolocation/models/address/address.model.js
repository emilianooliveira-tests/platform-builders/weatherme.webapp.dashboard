
class Address {

  constructor ({
    city,
    state,
    country,
  }) {
    this.city = city;
    this.state = state;
    this.country = country;
  }

}

export default Address;
