
export default class Weather {
  constructor ({
    current,
    date,
    description,
    max,
    min,
  }) {
    this.current = current;
    this.date = date;
    this.description = description;
    this.max = max;
    this.min = min;
  }
}