
import Weather from './models/weather.model';
import { get } from '../../clients/http/http.client';

export const byGeolocation = async ({ latitude, longitude }) => {
  const response = await get({
    params: {
      appid: process.env.REACT_APP_OPEN_WEATHER_MAP_API_KEY,
      excludes: 'alerts,minutely,hourly',
      lat: latitude,
      lon: longitude,
      units: 'metric',
    },
    path: '/data/2.5/onecall',
    url: process.env.REACT_APP_OPEN_WEATHER_MAP_API,
  });

  const parsed = response.data.daily.map(w => new Weather({
    ...w.temp,
    date: new Date(w.dt * 1000),
    description: w.weather[0].main.toUpperCase(),
  }));
  parsed[0].current = response.data.current.temp;

  return parsed;
};
